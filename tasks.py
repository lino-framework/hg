from atelier.invlib import setup_from_tasks

ns = setup_from_tasks(
    globals(),
    use_dirhtml=True,
    tolerate_sphinx_warnings=False,
    # doc_trees = ['docs', 'dedocs', 'frdocs'],
    # blogref_url="http://hw.lino-framework.org",
    make_docs_command='./make_docs.sh',
    revision_control_system='git',
    intersphinx_urls=dict(docs="https://hosting.lino-framework.org"),
    cleanable_files=['docs/rss_entry_fragments/*', 'docs/.build'])
