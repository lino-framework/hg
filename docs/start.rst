.. _hg.start:

==================
Installation
==================

.. contents::
   :depth: 1
   :local:


Setting up a Lino server
========================

.. toctree::
    :maxdepth: 1

    root
    install
    install_demo
    heroku

Setting up a Lino site
======================

.. toctree::
    :maxdepth: 1

    ref/settings
    settings
    dbengine
    email
    multiple_frontends
    certbot
    security
