===============================
Setting up a private git server
===============================

.. contents::
   :depth: 1
   :local:

Nowadays Github and Gitlab are very popular sites for managing software
source code and related issues. However, one may want to set up a private
git server. The steps necessary to create a repository on a linux server are
presented below::

  $ sudo adduser git
  $ sudo su git
  $ cd
  $ mkdir .ssh
  $ chmod 700 .ssh
  $ touch .ssh/authorized_keys
  $ chmod 600 .ssh/authorized_keys
  $ mkdir privatedata.git
  $ cd privatedata.git
  $ git init --bare

Now, each user's public ssh-key should be added to authorized_keys.

On client side, the following steps should be done::

  $ mkdir privatedata
  $ cd privatedata
  $ git init
  $ touch README
  $ git status
  $ git remote add origin git@yourgitserver.net:/home/git/privatedata.git
  $ git add .
  $ git push origin HEAD:master
  $ git push origin master --force
  $ git commit -m "initial commit"
  $ git push origin master
  $ git status
  $ nano README        // add some text and save
  $ git commit -a
  $ git status
  $ git push --set-upstream origin master


To check, whether the configuration works::

  $ cd /tmp
  $ git clone git@yourgitserver.net:/home/git/privatedata.git
  $ ls privatedata

One should see README in /tmp/privatedata.
