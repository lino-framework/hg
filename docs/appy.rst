============================
Lino and LibreOffice
============================

See also :doc:`oood`.


Troubleshooting
===============

- :message:`An error occurred during the conversion. Couldn't not connect to
  LibreOffice on port 8100. Connector : couldn't connect to socket (Connection
  refused) at ./io/source/connector/connector.cxx:118`

  comes when the LibreOffice service is down. See :ref:`admin.libreoffice`.

- :message:`'NoneType' object has no attribute 'getCurrentController'`

  indicates that something has gone wrong while building the document. This came
  e.g. when :mod:`appy` 1.0.17 was being used together with LibreOffice 24.2.

- :message:`Warning: failed to launch javaldx - java may not function correctly`

  Solution: :cmd:`sudo apt-get install default-jre libreoffice-java-common`.
