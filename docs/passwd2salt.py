from crypt import crypt, mksalt, METHOD_SHA512
from getpass import getpass

password = getpass("Your password:")
print(crypt(password, mksalt(METHOD_SHA512)))
