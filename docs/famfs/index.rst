============================
Running a family file server
============================

Installation
============

- Buy a RaspberryPi and install a virgin Debian on it
- Add the following modules and configs: (backup, vtun, vnc, owncloud)

Usage
=====

Make backups using rsync
------------------------

In order to mount the Pi, run :file:`pi_mount.sh`::

  #!/bin/bash
  set -e
  TARGET=/mnt/pi
  WHAT=luc@192.168.1.234

  if [ -d $TARGET ] ; then
      echo "Mounting $WHAT to $TARGET..."
  else
      echo "Cannot mount $WHAT because $TARGET is not a directory"
      exit -1
  fi
  sshfs $WHAT:/ $TARGET


Then run :xfile:`backup2pi.sh`, which is the same as :xfile:`backup2exthd.sh`
except that TARGET is :file:`/mnt/pi/var/backups/luc`.


Access home server using Vtun
-----------------------------

One could buy some cloud instance with public IP address to access the home
RaspberryPi using Vtun. The vtund configuration file for cloud instance (server
side) :file:`/etc/vtund.conf`::

  options {
     port 1234;
     ifconfig   /usr/sbin/ifconfig;
     route      /usr/sbin/route;
  }
  # Default session options
  default {
     type tun;
     proto tcp;
     compress no;   # Compression is off
     encrypt yes;   # ssh does the encryption
     speed 0;       # By default maximum speed
     keepalive yes;
     stat yes;
  }
  tunnel1 {
     device tun1;
     encrypt yes;
     keepalive yes;
     passwd  SomeSecretPassword;    # Change it!
     up {
        ifconfig  "%% 10.3.1.1 pointopoint 10.3.1.2";
        route "add gw 10.3.1.2 dev tun1";
     };
     down{
        ifconfig "%% down";
     };
  }

To :file:`/etc/hosts` add::

  10.3.1.2  home

The systemd unit file for Vtun server is :file:`/etc/systemd/system/vtund.service`::

  [Unit]
  Description=virtual tunnel daemon
  After=syslog.target

  [Service]
  Type=forking
  ExecStart=/usr/sbin/vtund -s

  [Install]
  WantedBy=multi-user.target

For RaspberryPi (client side) add :file:`/etc/vtund.conf`::

  options {
     port 1234;
     ifconfig   /sbin/ifconfig;
     route      /sbin/route;
  }
  # Default session options
  default {
     type tun;
     proto tcp;
     encrypt yes;
     speed 0;
     keepalive yes;
     stat yes;
  }
  tunnel1 {
     device tun1;
     encrypt yes;
     keepalive yes;
     passwd SomeSecretPassword;    # Change it!
     up {
        ifconfig  "%% 10.3.1.2 pointopoint 10.3.1.1";
        route "add gw 10.3.1.3 dev tun1";
     };
     down{
        ifconfig "%% down";
     };
  }

The systemd unit file for Vtun client :file:`/etc/systemd/system/vtund.service`::

  [Unit]
  Description=Virtual tunnel
  After=syslog.target

  [Service]
  Type=forking
  ExecStart=/usr/sbin/vtund -p -f /etc/vtund.conf tunnel1 yourcloudinstance.net

  [Install]
  WantedBy=multi-user.target


VNC
---

Open the :file:`/root/.vnc/config.d/vncserver-x11` config file.
Replace Authentication=SystemAuth with Authentication=VncAuth and save the file.
Run sudo vncpasswd -service. This will prompt you to set a password
and will insert it for VNC Server running in Service Mode.
After that restart the VNC Server.

ownCLoud
--------

From owncloud.com download tarball and extract it to /var/www.
Because owncloud uses some database, choose for example mysql (mariadb).
Configure database name: ownclouddb, database user: piuser and
database passwd: pipass. Owncloud config file resides in
:file:`/var/www/owncloud/config/config.php`::

  array (
      0 => 'localhost',
      1 => '192.168.1.10',
    ),

Instead 1, put your own RaspberriPi IP address. Also, apache configuration
should be changed: :file:`/etc/apache2/sites-enabled/000-default.conf`::

  <VirtualHost *:80>
  	ServerAdmin webmaster@localhost
    #DocumentRoot /var/www/html
    DocumentRoot /var/www/owncloud
    #DirectoryIndex index.php
    #ErrorLog /var/log/apache2/owncloud.log
  	ErrorLog ${APACHE_LOG_DIR}/error.log
  	CustomLog ${APACHE_LOG_DIR}/access.log combined
  </VirtualHost>



Discussion
==========


How to share my file collections as read-only to family members
---------------------------------------------------------------

First answer: seems that in ownCLoud it is not possible to create a symbolic
link to some other directory (`src
<https://central.owncloud.org/t/how-to-make-owncloud-reading-filesystem-symlinks/1802>`__).
