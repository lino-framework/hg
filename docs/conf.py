# -*- coding: utf-8 -*-
# fmt: off

import datetime

from atelier.sphinxconf import configure; configure(globals())
from lino.sphinxcontrib import configure; configure(globals())

project = 'Lino Hosting Guide'
html_title = "Hosting Guide"
copyright = '2008-{} Rumma & Ko Ltd'.format(datetime.date.today().year)

extensions += ['lino.sphinxcontrib.logo']

html_context.update({
    'display_gitlab': True,
    'gitlab_user': 'lino-framework',
    'gitlab_repo': 'hg',
})

html_use_index = True

rst_prolog = """
:doc:`/start` | :doc:`/topics/index` | :doc:`/beyond`
"""
