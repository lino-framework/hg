==================
Systemd Cheatsheet
==================

.. contents::
   :depth: 1
   :local:

Debian adopted systemd in 2015 as the new init system. Systemd is
responsible for running different system services.

To start (also stop, restart) a specific systemd service (e.g. nginx)::

  systemctl start nginx

Enabling and disabling of the aforementioned service::

  systemctl enable nginx; systemctl disable nginx

List all possible systemd services in a Linux server::

  systemctl list-units --type=service

If we want to display all running systemd services as a tree::

  systemctl status

To display the status of some specific service::

  systemctl status nginx

Closely connected with systemctl is journalctl - the command for
inspecting log-files. To get some information about some system service::

  journalctl -xe | grep service_name

The main site of systemd with detailed information:

- https://systemd.io

To study more about systemd, there is a freely downloadable book:

- https://opensource.com/downloads/pragmatic-guide-systemd-linux

3 surprising things Linux sysadmins can do with systemd:

- https://opensource.com/article/23/3/3-things-you-didnt-know-systemd-could-do
