.. _getlino.install.demo:

======================================
Set up a public demo server
======================================

.. _pip: http://www.pip-installer.org/en/latest/
.. _virtualenv: https://pypi.python.org/pypi/virtualenv

A demo server is similar to a production server, but all the sites share a same
environment and certain services are disabled.

..
  Warning : This is the deeper Python jungle. Don't try this before you have
  installed a few contributor environments and :doc:`production servers
  <install>`.

Set up a :term:`master environment` as described in
:ref:`getlino.install.master`, and then run :cmd:`getlino configure` as
follows::

   # getlino configure --clone --web-server nginx --server-domain <YOUR_PUBLIC_DOMAIN_NAME> --https --monit --db-engine mysql --db-user demo --db-password demo

The ``--clone`` option is optional, it means to clone all repositories and to
use the latest *development* versions. Without that option, your demo server
will show the *released* versions.

Manually edit the :xfile:`pull.sh` in your master environment. A
:xfile:`pull.sh` does much more on a demo server than on a production server: it
also runs :cmd:`pm prep` on each demo site. See :cmd:`less \`which pull.sh\`` on
an existing demo server for inspiration.

Remove :xfile:`make_snapshot.sh` and linod entries from cron and supervisor.

Multiple versions
=================

Instead of using a single virtualenv called ``master``, you may use multiple
shared environments on your demo server.

You may create other shared virtualenvs by changing the branch and clone another
set of repositories::

   # getlino configure --shared-env /usr/local/lino/shared/latest --clone
   # getlino configure --shared-env /usr/local/lino/shared/pypi

Specify ``--shared-env`` when creating demo sites::

   # getlino startsite noi first --shared-env /usr/local/lino/shared/latest
   # getlino startsite tera second --shared-env /usr/local/lino/shared/pypi


How to upgrade a demo server
============================

Simply sign in as a :term:`server administrator` and run :xfile:`pull.sh`.
