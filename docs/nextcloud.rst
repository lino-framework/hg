=========================
Our Nextcloud cheat sheet
=========================

Here we present some details to set up **Nextcloud** in Debian 10.

We should install some web-server, for example nginx: ::

  $ sudo apt install nginx

As nextcloud needs PHP, install some PHP packages: ::

  $ sudo apt-get install -y php php-gd
  $ sudo apt-get install -y php-zip php-dom php-xml php-curl
  $ sudo apt-get install -y php-simplexml php-mbstring

Nextcloud stores data into database and we can choose between
mariadb, postgresql and sqlite. We take the first one: ::

  $ sudo apt install mariadb-server php-mysql

After installation we should set the root password for database server
and add the password to password manager: ::

  $ sudo mysql -u root -p

For nextcloud, create the specific database, the database user and password: ::

  CREATE DATABASE nextclouddb;
  GRANT ALL ON nextclouddb.* TO 'linostuff'@'localhost' IDENTIFIED BY 'pass';
  FLUSH PRIVILEGES;
  EXIT;

Add these credidentals also to password manager.

Further, download nextcloud source: ::

  $ cd /tmp
  $ wget https://download.nextcloud.com/server/releases/latest-21.tar.bz2
  $ bunzip2 latest-21.tar.bz2
  $ tar xvf latest-21.tar
  $ sudo mv nextcloud/ /var/www/html/
  $ cd /var/www/html
  $ sudo chown -R www-data:www-data nextcloud
  $ sudo chmod -R 755 nextcloud
  $ sudo apt install php-fpm
  $ sudo systemctl status php7.3-fpm
  $ sudo nano /etc/nginx/sites-available/nextcloud.conf
  $ sudo ln -s /etc/nginx/sites-available/nextcloud.conf /etc/nginx/sites-enabled/
  $ sudo systemctl restart nginx.service


We adapted for us the nextcloud nginx-config:
https://docs.nextcloud.com/server/17/admin_manual/installation/nginx.html::


   upstream php-handler {
        server 127.0.0.1:9000;
        server unix:/var/run/php/php7.3-fpm.sock;
   }

   server {
        listen 80;
        server_name 192.168.0.1;
        # enforce https
        return 301 https://$server_name$request_uri;
   }

   server {
        listen 443 ssl;
        server_name 192.168.0.1;
        ssl_certificate     /etc/nginx/cert/nextcloud.crt;
        ssl_certificate_key /etc/nginx/cert/nextcloud.key;
        # Path to the root of your installation
        root /var/www/html/nextcloud/;
   }
