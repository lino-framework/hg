.. _hg.ref.settings:

=======================================
Site settings reference for maintainers
=======================================

.. currentmodule:: lino.core.site


.. contents::
   :depth: 1
   :local:

Some common site settings
=========================

The following settings of a Lino site are defined as class attributes of
:class:`lino.core.site.Site`.

.. setting:: languages

  The :term:`language distribution` to be used on this site.

  This must be either `None`, or an iterable of :term:`language codes
  <language code>`, or a string containing a space-separated series of
  language codes.

  Examples::

    languages = "en de fr nl et"
    languages = ['en', 'fr']
    languages = 'en'

  The first language in this list is the site's default language,
  returned by :meth:`get_default_language`.

.. setting:: use_help

  Whether this site uses :mod:`lino.modlib.help`.

  Default value is ``False``.

.. setting:: use_ipdict

  Whether this site uses :mod:`lino.modlib.ipdict`.

  Default value is ``False``.

  Note that :mod:`lino.modlib.ipdict` unlike normal plugins should
  not be installed by adding it to your :meth:`get_installed_plugins`
  method but by setting this attribute.  This approach has the
  advantage of also setting :setting:`MIDDLEWARE_CLASSES`
  automatically.

.. setting:: default_ui

  The full Python name of the plugin that acts as main :term:`front end` on
  this site.

  Default value is :mod:`lino.modlib.extjs`. Other candidates are
  :mod:`lino_react.react`,
  :mod:`lino.modlib.bootstrap3`,
  :mod:`lino_openui5.openui5`,
  :mod:`lino_xl.lib.pages` and
  :mod:`lino_extjs6.extjs6` .

  Another possibility is to set it to `None`. In that case you must
  probably also set :attr:`root_urlconf` to a custom URL dispatcher.


Advanced site settings
======================

.. setting:: hidden_languages = None

  A string with a space-separated list of :term:`language codes <language
  code>` that should be hidden. This is useful when you have multilingual
  database content and a :term:`language distribution` of more than three
  languages.

.. setting:: webdav_protocol

  Set this to a string like e.g. 'wdav' to instruct Lino to use this custom
  protocol instead of ``http`` when linking to "editable" printable documents.

  See :doc:`/webdav`.

.. setting:: webdav_url

  The URL location for webdav files.  In a normal production configuration
  you should leave this unchanged and Lino will set the  default value
  ``'/media/webdav/'``. You also need to configure your web
  server to actually serve the files below this location using the WebDAV
  protocol. See :doc:`/webdav`.

  Obsolete: This may be used to simulate a :term:`WebDAV` location on a
  development server.  For example on a Windows machine, you may set
  it to ``w:\``, and before invoking :manage:`runserver`, you issue in
  a command prompt::

      subst w: <dev_project_path>\media\webdav

.. setting:: webdav_root

  The path on server to store webdav files.
  Default is :attr:`Site.site_dir` + ´/media/webdav'.

.. setting:: custom_layouts_module

  The name of the **custom layouts module** to be used on this site.  Default
  value is defined by the :term:`application developer`.

.. setting:: workflows_module

  The name of the **workflows module** to be used on this site.  Default
  value is defined by the :term:`application developer`.

.. setting:: user_types_module

  The name of the **user types module** to be used on this site.  Default
  value is defined by the :term:`application developer`.

  For example::

      class Site(Site):
          user_types_module = 'myapp.user_types'

.. setting:: use_security_features = False

  Set this to `True` in order to activate a selection of security
  features to protect against miscellaneous attacks.  You can do
  this only if your application is being served via HTTPS.  The idea
  is to provide a reasonable security out of the box.

  This will activate some middleware and set some security-related
  settings.  This is a new feature and not much tested.  As a hoster
  you may prefer adding security manually using your established
  standards (regarding security Lino does not add anything to plain
  Django).  See also :doc:`/security`.

.. setting:: root_urlconf = 'lino.core.urls'

  The value to be assigned to :setting:`ROOT_URLCONF` when this :class:`Site`
  instantiates.

  The default value is :mod:`lino.core.urls`.

.. setting:: social_auth_backends = None

  A list of backends for `Python Social Auth
  <https://github.com/python-social-auth>`__ (PSA).

  Having this at a value different from `None` means that this site
  uses authentication via third-party providers.

  Sites which use this feature must also install PSA into their environment
  (which is done automatically by :manage:`install`).

  Depending on the backend you must also add credentials in your
  local :xfile:`settings.py` file, e.g.::

    SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = \
      '1234567890-a1b2c3d4e5.apps.googleusercontent.com'
    SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'SH6da...'

  A working example is in the :mod:`lino_book.projects.noi1e` demo
  project.

.. setting:: appy_params

  Default value is  `dict(ooPort=8100, pythonWithUnoPath='/usr/bin/python3',
  raiseOnError=True)`.

  Used by :class:`lino_xl.lib.appypod.choicelist.AppyBuildMethod`.

  Allowed keyword arguments for `appy.pod.renderer.Render` are::

    pythonWithUnoPath=None,
    ooPort=2002
    stylesMapping={}
    forceOoCall=False,
    finalizeFunction=None
    overwriteExisting=False
    raiseOnError=False
    imageResolver=None

  See `the source code
  <http://bazaar.launchpad.net/~appy-dev/appy/trunk/view/head:/pod/renderer.py>`_
  for details.

  See also :doc:`/oood`

Common issues you can fix using your :xfile:`settings.py`
=========================================================

Activate :term:`ElasticSearch`: see :setting:`use_elasticsearch`  and
:setting:`search.elasticsearch_url`.

Configure the columns to print in the body of an invoice when using the
``appy_pod`` :term:`build method` (:doc:`/oood`): see
:setting:`sales.print_items_table`.

Whether this site accepts user accounts that are linked to some third-party
authentication provider (Google, Facebook, Wikipedia, ...). See
:setting:`users.third_party_authentication`.
