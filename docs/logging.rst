.. _host.logging:

=============
About logging
=============

This document explains logging to a :term:`system administrator` of a Lino
:term:`production site`.

Lino comes with a sophisticated framework for logging, which gets activated
automatically. You can choose to not use it by defining your own
:setting:`LOGGING` and :setting:`LOGGING_CONFIG` settings.

We assume that you have read the Django's doc about `Logging
<https://docs.djangoproject.com/en/5.0/topics/logging/>`__.

Developers will also want to read :ref:`dg.topics.logging`.

The ``log`` directory
=====================

When a Lino process starts up, it checks whether there is a subdirectory named
:xfile:`log` in the :term:`local site directory`.  If such a directory exists,
Lino automatically activates file logging to a file named :xfile:`lino.log` in
that directory. The :xfile:`log` directory may be a symbolic link to a directory
below :file:`/var/log/`.

.. xfile:: log

  A subdirectory of a Lino site's project directory that contains the
  :xfile:`lino.log` file.

.. xfile:: lino.log

  The name of Lino's logger file.

  Default value is :xfile:`lino.log`. You can change this name by setting the
  :attr:`logger_filename <lino.core.site.Site.logger_filename>` attribute of
  your :class:`Site <lino.core.site.Site>` class.

  Until 20160729 it was :xfile:`system.log`.

On a production site you can have multiple processes running on a same
site at the same time, which can lead to conflicts because these processes write
to a same :xfile:`lino.log` file.  That's why we also have a :term:`log server`.

.. glossary::

  log server

    A background process used when multiple processes need to write to a same
    :xfile:`lino.log` file on a production site. Lino

Lino provides a built-in :term:`log server`, which you must start by running the
:manage:`linod` admin command.


Lino logging configuration
==========================

Some settings influence logging:

- :attr:`lino.core.site.Site.history_aware_logging`
- :attr:`lino.core.site.Site.logger_filename`
- :attr:`lino.core.site.Site.auto_configure_logger_names`
- :attr:`lino.core.site.Site.log_each_action_request`

- :meth:`lino.core.site.Site.setup_logging`



Setting the logging level via the environment
=============================================

You can use environment variables :envvar:`LINO_LOGLEVEL`
:envvar:`LINO_FILE_LOGLEVEL` to control the logging level.

.. envvar:: LINO_LOGLEVEL

The logging level to use for both console and file handlers. It should
be one of `INFO`, `DEBUG` etc.

.. envvar:: LINO_FILE_LOGLEVEL

The logging level to use for file handlers (the :xfile:`lino.log` file) when you
want this to be different from  :envvar:`LINO_LOGLEVEL`. It should be one of
`INFO`, `DEBUG` etc.


.. _logrotate:

Configuring logrotate
=====================


To activate logging to a file, you simply add a symbolic link named
:xfile:`log` which points to the actual location::

    $ sudo mkdir -p /var/log/lino/
    $ sudo chown :www-data /var/log/lino/
    $ sudo chmod g+ws /var/log/lino/
    $ sudo mkdir /var/log/lino/prj1/
    $ cd ~/mypy/prj1/
    $ ln -s /var/log/lino/prj1/ log/


We recommend a file :file:`/etc/logrotate.d/lino` with something like::

    /path/to/lino_sites/prod/log/lino.log {
            weekly
            missingok
            rotate 156
            compress
            delaycompress
            notifempty
            create 660 root www-data
            su root www-data
            sharedscripts
    }


After changes in the config you can tell logrotate to force them::

  $ sudo logrotate -f /etc/logrotate.d/lino



.. _log2syslog:

Logging all bash commands to syslog
===================================


Add the following to your system-wide :file:`/etc/bash.bashrc`:

.. literalinclude:: log2syslog
