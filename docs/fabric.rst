==================================================
Using Fabric to get quick overview of linux system
==================================================

One can use python module Fabric to get some quick information about your linux system.
First, look at https://docs.fabfile.org/en/stable/index.html and install it: ::

   $ pip install -U fabric

Now launch python and test if it works on the simplest case:

.. code-block:: python

   >> from fabric import Connection;
   >> result = Connection('yourusername@someserver.org').run('uname -a', hide=True);
   >> print(result);

In case ssh-key is protected with password, use: ::

   $ eval $(ssh-agent); ssh-add

Further, you may create a python script to check the state of several servers at once:

.. code-block:: python

   import getpass
   from fabric import Connection;
   hosts = ["server1.org", "server2.org", "server3.org", "server4.org"]
   commands = ["uname -a", "last -n 10", "free -m", "df -h", "netstat -tuan"]
   connection_args = dict(port=22, user=getpass.getuser())
   run_args = dict(hide=False)
   for h in hosts:
   print("Server:", h);
   conn = Connection(host=h, **connection_args)
   for cmd in commands:
      print("Command:", cmd);
      res = conn.run(cmd, **run_args)
      print()
   print()

Save this script to servermonitor.py and run: ::

   $ python servermonitor.py

It is supposed that you have the same username in your local machine and in remote servers.
You can modify the commands-line according to your needs.
