========================
Some bash aliases we use
========================

The Lino repository contains a file `/bash/bash_aliases
<https://gitlab.com/lino-framework/lino/blob/master/bash/bash_aliases>`__,
which is meant as a template for your own :xfile:`.bash_aliases`.

- :cmd:`go`
- :cmd:`pywhich`
