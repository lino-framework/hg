========
Glossary
========

Not yet sure where to put the following.

.. glossary::
  
  multilingual database content

    A Lino-specific feature where some database tables contain names or
    designations in more than one language.
