.. _hg.email:

====================================
The email settings of a Lino site
====================================

Most :term:`Lino applications <Lino application>` send emails to :term:`site
users <site user>` or :term:`business partners <business partner>` as part of
their functionality. Here is an overview of the settings related to sending
emails to the outside world.

These settings are usually stored in the :term:`local settings module`.

.. glossary::

  local settings module

    A module meant to contain Django settings to be shared to multiple sites on
    this machine, but only on this machine. It is conventionally named
    :mod:`lino_local.settings`.


.. contents::
   :depth: 1
   :local:


Reference
=========

Here are the Django settings for sending emails.

.. setting:: ADMINS

  A list with one or more email addresses of :term:`server administrators
  <server administrator>` to be notified when an exception happens on the
  server.

.. setting:: EMAIL_HOST

  The SMTP host that will accept outgoing mails from this site.

  See also https://docs.djangoproject.com/en/5.0/ref/settings/#email-host

.. setting:: EMAIL_PORT

  The port to use when submitting outgoing mails from this site to
  :setting:`EMAIL_HOST`.

  See also https://docs.djangoproject.com/en/5.0/ref/settings/#email-port

.. setting:: EMAIL_HOST_USER

  The user name to use when connecting to the :setting:`EMAIL_HOST`. If this is
  empty. Django will try to submit anonymously.

  See also https://docs.djangoproject.com/en/5.0/ref/settings/#email-host-user

.. setting:: EMAIL_HOST_PASSWORD

  The password to use when connecting as :setting:`EMAIL_HOST_USER` to the
  :setting:`EMAIL_HOST`.

  See `How to store secret credentials`_

  See also https://docs.djangoproject.com/en/5.0/ref/settings/#email-host-password

.. setting:: SERVER_EMAIL

  The address to use as sender in outgoing mails to the admins

.. setting:: DEFAULT_FROM_EMAIL

  Default value for sender of outgoing emails
  when application code doesn't specify it.

.. setting:: EMAIL_SUBJECT_PREFIX

  The subject prefix to use for emails to the :setting:`ADMINS`.

  See `Django docs
  <https://docs.djangoproject.com/en/5.0/ref/settings/#std:setting-EMAIL_SUBJECT_PREFIX>`__

  Lino also uses this in :mod:`lino.modlib.notify`.

.. setting:: EMAIL_USE_SSL

  Start an SMTP connection that is secure from the beginning using
  :class:`smtplib.SMTP_SSL`.

  See `Django docs
  <https://docs.djangoproject.com/en/5.0/ref/settings/#std:setting-EMAIL_USE_SSL>`__

.. setting:: EMAIL_USE_TLS

  Start an unsecured SMTP connection and then use :meth:`smtplib.SMTP.starttls`
  to encrypt it. This is the more secure successor of :setting:`EMAIL_USE_SSL`.

  See `Django docs
  <https://docs.djangoproject.com/en/5.0/ref/settings/#std:setting-EMAIL_USE_TLS>`__

.. setting:: EMAIL_SSL_KEYFILE

.. setting:: EMAIL_SSL_CERTFILE

  Both :setting:`EMAIL_SSL_KEYFILE` and :setting:`EMAIL_SSL_CERTFILE` may be set.
  When you have a keyfile, then you must also have a certfile. When none is set,
  Django will use :meth:`create_default_context` (which is recommended according
  to Python `Security considerations
  <https://docs.python.org/3/library/ssl.html#ssl-security>`__)


How to store secret credentials
===============================

Install the `dotenv <https://pypi.org/project/python-dotenv/>`__ Python
package::

  pip install python-dotenv

Create a file :Xfile:`~/.env` with this content::

  EMAIL_HOST_USER=joe
  EMAIL_HOST_PASSWORD=kkFUW8TRSXVgeq

In your :xfile:`settings.py` file::

  import dotenv; secrets = dotenv.dotenv_values()
  EMAIL_HOST_USER = secrets['EMAIL_HOST_USER']
  EMAIL_HOST_PASSWORD = secrets['EMAIL_HOST_PASSWORD']


Helo command rejected: need fully-qualified hostname
====================================================

You may receive this error message when sending from a machine that has no
public FQDN to an SMTP server that has strict `smtpd_helo_restrictions
<https://www.postfix.org/postconf.5.html#smtpd_helo_restrictions>`__.

Here is a hack to make Django use your IP address rather than
:func:`socket.getfqdn` when sending email. Add the following to your
:xfile:`settings.py` file::

  from socket import gethostname, gethostbyname
  from django.core.mail import DNS_NAME
  ip_addr = gethostbyname(gethostname())
  DNS_NAME._fqdn = "[{}]".format(ip_addr)
  print("Using your IP address [{}] as FQDN in HELO".format(ip_addr))
