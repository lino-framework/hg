========================
Our ``cron`` cheat sheet
========================

.. command:: cron

On a :term:`production server` that has been installed using :ref:`getlino`,
:cmd:`cron` jobs should have been installed automatically as needed. But you might want
to optimize them or to set up other cron jobs.


- :cmd:`getlino configure` creates a :file:`getlino-certbot.conf` file in :file:`/etc/cron.d`
- Every :cmd:`getlino startsite` creates
  a :file:`make_snapshot_xxx` file in :file:`/etc/cron.daily` 


You may want to define when your daily and weekly :cmd:`cron` jobs run. The
default for daily jobs is **6:25am** in your server's timezone. To change this
value, edit your :file:`/etc/crontab` file::

  # Example of job definition:
  # .---------------- minute (0 - 59)
  # |  .------------- hour (0 - 23)
  # |  |  .---------- day of month (1 - 31)
  # |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
  # |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
  # |  |  |  |  |
  # *  *  *  *  * user-name command to be executed
  17 *    * * *   root    cd / && run-parts --report /etc/cron.hourly
  25 3    * * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
  47 3    * * 7   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
  52 3    1 * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )



Question: My job works correctly when run at the command line but fails when
it's run by cron. Answer:

- The files in a :file:`/etc/cron.*/` directory must be owned by ``root`` and
  (because they are launched via :cmd:`run-parts`) their name "must consist
  entirely of ASCII upper- and lower-case letters, ASCII digits, ASCII
  underscores, and ASCII minus-hyphens". Other files and directories are
  silently ignored.

- Look at your :file:`/var/log/syslog`

- Does the command contain a relative path? Paths in cron jobs should be
  absolute.

- Does the command use advanced bash features? Cron uses
  :file:`/bin/sh` by default.

- Does the job use environment variables? Cron does
  not load files like :xfile:`.bashrc`.

- The command must be executable by the user that cron is running your job as.
