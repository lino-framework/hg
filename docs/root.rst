=======================
Providing a Lino server
=======================

.. contents::
   :depth: 1
   :local:

The job of a server provider
============================

As a :term:`server provider` you are responsible for installing and maintaining
a :term:`Lino server`, i.e. a virtual or physical machine with a Linux operating
system.

You do this for a :term:`site operator`, who is usually your customer or your
employer. The :term:`site operator` designates one or several :term:`site
maintainers <server administrator>` who will install and maintain one or several
:term:`Lino sites <Lino site>` on that machine.

You hold root access to the server and create user accounts with ``sudo`` rights
for each :term:`server administrator`. You configure secure remote shell access (SSH)
of the server provide support to the server administrators.

You are responsible for providing backup service, monitoring and general
availability of the server as a whole.

You are *not* responsible for installing and maintaining specific system
packages, Lino source code and configuration, nor for giving :term:`end-user
support` to the  users of any :term:`Lino site` hosted on this site.



Where to get a virtual server
=============================

If you don't have your own in-house hardware or dedicated server, you can get a
Virtual Private Server from many providers. Here is a list of VPS providers we
have tested:

- https://www.ovh.ie/order/vps   3€/month
- https://www.hetzner.com/cloud  2.89€/month
- https://mochahost.com/vps.php  6.94€/month (Up to 50% OFF)


System requirements for a Lino site
===================================

We recommend a `stable Debian <https://www.debian.org/releases/stable/>`__ as
operating system.  Currently this means Debian 10 "Buster".

**One CPU** should be enough for a site with a few dozens of users.

You need **at least 10 GB of disk space**. You can see how much disk space you have
by saying::

  $ df -h

We recommend **at least 2GB of RAM** (because we didn't yet test production
sites with less).  How to see how much memory you have::

  $ free -h

In case you need help, a good thing is to report some diagnostic information
about your environment::

  $ cat /etc/debian_version

Preparing a new server
======================

Before creating system users, the root user should check the following.

In your :file:`/etc/ssh/sshd_config` make sure that ``PasswordAuthentication``
is set to ``no``.  We require server administrators to have a
:xfile:`~/.ssh/authorized_keys` file. They will need their password only for
running `sudo` commands.

Also run::

  # apt-get update && apt-get upgrade

The system should have installed the `sudo` package::

  # apt-get install sudo


Creating a user account
=======================

As a root user you will create a user account for every :term:`server administrator`.

In the following examples we assume that the user account to create is ``joe``.

Agree upon a temporary password with Joe (who can later change their password
using :cmd:`passwd`), and then type::

  # adduser joe

Server administrators must be members of the `sudo` and `www-data` groups::

  # adduser joe sudo
  # adduser joe www-data

Creating the user's :xfile:`~/.ssh/authorized_keys` file with the maintainer's
public ssh key::

  # su - joe
  $ mkdir .ssh && chmod 700 .ssh
  $ touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys
  $ cat >> .ssh/authorized_keys

Paste the administrator's public key to the terminal.  Press :kbd:`ENTER` to add
at least one newline.  Press :kbd:`Ctrl+D` to say you're finished with pasting
content.

Footnotes:

- `useradd` is a native binary compiled with the system, while `adduser`
  is a perl script that uses `useradd` in back-end.

- ssh requires that the :xfile:`.ssh` directory and its content should have
  permissions set so that only the owner can read, write, or open them.


How to generate a SSH key pair
==============================

In order to sign in via ssh as a :term:`server administrator` to a Lino server,
the server provider must add your public ssh key to the
:file:`.ssh/authorized_keys` file of your user account.

You can generate a pair of ssh keys (public and private) using the command
:cmd:`ssh-keygen -t rsa`.

Never give your private key to anybody else!

When ssh-agent is installed, generating a pair of ssh keys will automatically
store this identity on this computer.

How to list all identities on your computer: :cmd:`ssh-add -l`

How to register an identity on your computer when you have restored your key
pair from your old to your new computer: :cmd:`ssh-add /path/to/private_key`


How to copy all users from an existing server
==============================================

You can copy the user accounts of an existing Lino server to a newly created
server by saying something like::

  # export SRC=root@12.34.56.78
  # scp $SRC:/home/user1/.ssh /home/user1/
  # scp $SRC:/etc/shadow /etc/shadow

How to change the hostname
==========================

Every server has a "hostname", a relatively short "nickname" to designate it.
The hostname also appears in the prompt (unless somebody customized their prompt).
The hostname is not the same as the FQDN.

How to change the hostname of a Lino server::

  $ sudo hostnamectl set-hostname newname

Also edit your :file:`/etc/hosts` file.

If you use `mailutils
<http://mailutils.org/manual/html_node/configuration.html>`__, you must also check
your :file:`/etc/mail/local-host-names` file.

If that file doesn't exist, try::

  $ mail --show-config-options | grep SYSCONFDIR
  SYSCONFDIR=/etc 	- System configuration directory

Which means that actually the config files are in :file:`/etc/mail`. And one of
them, :file:`/etc/mail/local-host-names` contains my default ``From`` header.
