.. _admin.mailman:

=============
Using Mailman
=============

**Mailman** is a mailing lists manager, an optional component on a :doc:`mail
server <index>`.

.. contents::
   :depth: 1
   :local:



Mailman 2
=========

Install mailman::

  $ sudo apt install mailman

Configuration modifications for :xfile:`/etc/mailman/mm_cfg.py` are::

  from Defaults import *
  MAILMAN_SITE_LIST = 'mailman'
  #DEFAULT_URL_PATTERN = 'http://%s/cgi-bin/mailman/'
  DEFAULT_URL_PATTERN = 'https://%s/mailman/'
  IMAGE_LOGOS         = '/images/mailman/'
  DEFAULT_EMAIL_HOST = 'mydomain.org'
  #DEFAULT_URL_HOST   = 'mail.mydomain.org'
  DEFAULT_URL_HOST   = 'lists.mydomain.org'
  DEFAULT_SERVER_LANGUAGE = 'en'
  USE_ENVELOPE_SENDER    = 0
  DEFAULT_SEND_REMINDERS = 0
  SUBSCRIBE_FORM_SECRET = 'SomeSecretString'
  PRIVATE_ARCHIVE_URL = '/mailman/private'
  POSTFIX_STYLE_VIRTUAL_DOMAINS = ['mydomain.org']
  DEB_LISTMASTER = 'postmaster@mydomain.org'
  MTA = 'Postfix'
  VIRTUAL_HOSTS.clear ()
  add_virtualhost(DEFAULT_URL_HOST , DEFAULT_EMAIL_HOST )

Mailman entry in :xfile:`/etc/postfix/master.conf` is::

    mailman   unix  -       n       n       -       -       pipe
    flags=FR user=list argv=/usr/lib/mailman/bin/postfix-to-mailman.py
    ${nexthop} ${user}

For nginx, the configuration for :xfile:`lists.conf` should be like::

  server {
        server_name lists.mydomain.org;

            location /mailman {
            root  /usr/lib/cgi-bin;
            fastcgi_split_path_info (^/mailman/[^/]+)(/.*)$;
            fastcgi_pass  unix:///var/run/fcgiwrap.socket;
            include /etc/nginx/fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param PATH_INFO       $fastcgi_path_info;
            }
            location /images/mailman {
            alias /usr/share/images/mailman;
            }
            location /pipermail {
            alias /var/lib/mailman/archives/public;
            autoindex on;
            }

    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/lists.mydomain.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/lists.mydomain.org/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
  }

  server {
    if ($host = lists.mydomain.org) {
        return 301 https://$host$request_uri;
    }
    listen 80;
        server_name lists.mydomain.org;
    return 404;
  }



Set up a certbot certificate::

  $ sudo certbot --nginx -d yourdomain.org

Set a **site password** and the **list creator password** (`more details
<https://www.gnu.org/software/mailman/mailman-install/node44.html>`__)::

  $ sudo mmsitepass <your-site-password>
  $ sudo mmsitepass -c <list-creator-password>


Source:
https://www.nginx.com/resources/wiki/start/topics/recipes/mailman/




Mailman 3
=========

Mailman 3 is in active developement, and --like Lino-- uses Django for its web
interface.  The Mailman web interface consists of two Django apps, Postorious
(lists management) and Hyperkitty (list archives), which may be used separately,
or integrated into another Django project together with custom apps, or,
--probably the most frequent use case-- are combined into a Django project
called the "Mailman Suite".  This is what we are going to install here.

Should we use the packaged version by saying :cmd:`sudo apt install
mainman3-full`? It seems that we prefer to use selected clones of the official
git repository in order to be able to easily switch between versions. Though we
didn't yet fully explore the package approach (see :blogref:`20200813`).

The following is not finished.

We assume that you have already installed and configured postfix
(:doc:`postfix`) and that your basic email system is working.

Get the sources
===============

The mailman suite is like any other Lino site, except that the
:xfile:`settings.py` file is copied from
https://gitlab.com/mailman/mailman-suite.

We assume that the :term:`master environment` is installed (see
:ref:`lino.admin.install`).

Draft installation instructions with getlino::

  $ sudo env PATH=$PATH getlino startsite --db-engine=postgresql std lists

Manually install mailman repository::

  $ go mailman
  $ a
  $ mkdir env/repositories
  $ cd $_
  $ git clone https://gitlab.com/mailman/mailman.git
  $ pip install -e mailman/
  $ go mailman

Download config files from https://gitlab.com/mailman/mailman-suite::

  $ mv settings.py local_settings.py
  $ wget https://gitlab.com/mailman/mailman-suite/-/raw/master/mailman-suite_project/settings.py
  $ wget https://gitlab.com/mailman/mailman-suite/-/raw/master/mailman-suite_project/urls.py

Install Python wrappers for using memcached and the Whoosh backend for
HyperKitty archive search::

  $ pip install pylibmc Whoosh







Tips and tricks
===============

Add a new member on the command line

$ sudo add_members -r recipients.txt test



Troubleshooting
===============

The following simulates quite closely what mailman does when sending its mails.

::

  $ sudo apt install sendemail
  $ dig gmx.net MX
  $ sendemail -f mylist@abc.org -t myrcpt@gmx.net -u test -m test -s mail.ghi.org:25 -v -o tls=no

Sources
=======

- https://wiki.list.org/DOC/Howto_Install_Mailman3_On_Debian10
- https://gist.github.com/plepe/dab22fdbfec63d8632709065890124a3
