.. _hosting.dovecot:

=======
Dovecot
=======

Dovecot is an IMAP/POP3 server, which is needed if users of your server want to
access their mails using a mail client like Thunderbird.

Before installing Dovecot, you should have installed :doc:`Postfix <postfix>`.

Using hints from:

- https://tecadmin.net/install-dovecot-on-debian-10/


Installation
============

Install the Debian packages::

  $ sudo apt install dovecot-imapd dovecot-pop3d

Edit one line in your :file:`/etc/dovecot/dovecot.conf` file::

  listen = *, ::

Edit your :file:`/etc/dovecot/conf.d/10-auth.conf` file::

  disable_plaintext_auth = no
  auth_mechanisms = plain login

Edit your :file:`/etc/dovecot/conf.d/10-mail.conf` file::

  mail_location = maildir:~/Maildir

IMPORTANT: Mail location parameters should be in sync. Above means that the
postfix config parameter is ``home_mailbox = Maildir/``.

Edit your :file:`/etc/dovecot/conf.d/10-master.conf` file::

  unix_listener /var/spool/postfix/private/auth {
      mode = 0666
      user = postfix
      group = postfix
    }

Edit your :file:`/etc/dovecot/conf.d/10-ssl.conf` file::

  ssl = yes
  ssl_cert = </etc/letsencrypt/live/mail.example.com/fullchain.pem
  ssl_key = </etc/letsencrypt/live/mail.example.com/privkey.pem


Control the dovecot service::

  $ sudo systemctl enable dovecot.service
  $ sudo systemctl stop dovecot.service
  $ sudo systemctl start dovecot.service
  $ sudo systemctl status dovecot.service
