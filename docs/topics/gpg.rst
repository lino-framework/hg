===============
GPG cheat sheet
===============


How to sign a document
======================

We have a file foo.pdf.

Signer 1::

  opengpg




How to export a public key::

  $ gpg  --armor --export luc.saffre@gmx.net
  $ gpg  --fingerprint luc.saffre@gmx.net
