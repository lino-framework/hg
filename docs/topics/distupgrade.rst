======================================
Update a Lino site after dist-upgrade
======================================

After a dist-upgrade, the following procedure may help to upgrade a
:term:`virtualenv`.

On a demo server
================

On a demo server there is a :term:`virtualenv` named `master` and the path to
this environment is `/usr/local/lino/shared/env/master`.

#. Deactivate the environment and change directory into the environment's
   parent directory::

    $ deactivate
    $ cd /usr/local/lino/shared/env

#. Rename the old environment and create a new one of same name::

    $ mv master master1
    $ python -m venv master

#. Move the pull.sh and repositories from old to new env::

    $ mv master1/bin/pull.sh master/bin/pull.sh
    $ mv master1/repositories master/

#. Activate the new environment::

    $ source master/bin/activate

#. Run the pull script::

    $ pull.sh


On a production server
======================

Reinstall the master :term:`virtualenv` with getlino::

  sudo su
  deactivate
  cd /usr/local/lino/shared/env
  mv pilv pilv1
  python3 -m venv pilv
  . pilv/bin/activate
  pip install getlino

On each production site  ::

  mv env env1
  getlino startsite XXX yyy
  pip install atelier  # still needed
  Okay to reinstall cosi to /usr/local/lino/lino_local/abservices? [y or n] Yes



Troubleshooting
===============

Note down the current python version of the environment::

    $ cat master/pyvenv.cfg

The output should look something similar to the following below::

    home = /usr/local/lino/shared/env/master/bin
    include-system-site-packages = true
    version = 3.8.3

Take a note of the major and minor part of the version number (in our case: 3.8)

..
  Old stuff

  Step 03: Upgrade the environment directory to use the latest python::

      $ python -m venv --upgrade --system-site-packages master # replace master with the appropriate env name

  Step 04: List the packages in the old python environment::

      $ pip list --user -l --exclude-editable --format=freeze --path master/lib/python3.8/site-packages > requirements.txt
      $ # replace master with appropriate env name and 3.8 with appropriate version number that we got from step 02.

  Step 05: Remove old environment's site packages::

      $ rm -r master/lib/python3.8
      $ # replace master with appropriate env name and 3.8 with appropriate version number that we got from step 02.

  Step 07: Install the packages::

      $ pip install --user -r requirements.txt

  Step 08: Remove the requirements.txt::

      $ rm requirements.txt

  Step 09: Reinstall the editable packages (optional | perheps leave it to the developers)::

      $ pp pip install -e .

  The python environment should be upgraded at this point.
  Thank you.

  For managing python environments we recommend using the `venv
  <https://docs.python.org/3/library/venv.html>`__ package.
