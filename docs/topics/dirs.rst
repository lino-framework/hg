========================
Directories used by Lino
========================

.. glossary::

  project directory

    The directory that contains the central configuration files of your Lino
    site.

    The :term:`project directory` is the one that contains your
    :xfile:`manage.py` file. When you are in the project directory, you can type
    :cmd:`pm` to execute any :term:`django-admin command`.

    Stored in :attr:`setings.SITE.project_dir <lino.core.Site.project_dir>`

  local site directory

    The directory where Lino creates local files.
    This is the directory that contains your :xfile:`settings.py` file.

    Stored in :attr:`setings.SITE.site_dir <lino.core.Site.site_dir>`

The :term:`project directory` and the :term:`local site directory` are usually
the same, except for example when you use a **settings package**.
