#!/bin/sh
# set -e  # do *not* exit on error because we want mysql to restart even when
# the backup failed.
# Inspired from https://borgbackup.readthedocs.io/en/stable/quickstart.html#automating-backups

export BORG_REPO=myserver@backup-server.net:/home/myserver/backups
export BORG_PASSPHRASE=`cat /root/backup2borg_passwd`

echo "Make snapshots"
# service supervisor stop
/usr/local/lino/lino_local/first/make_snapshot.sh
/usr/local/lino/lino_local/second/make_snapshot.sh

echo "Stop mysql"
service mysqld stop

echo "Start backup"
borg create                         \
    --filter AME                    \
    --compression lz4               \
    --exclude-caches                \
    --exclude '/home/*/.cache/*'    \
    --exclude '/var/backups/*'      \
    --exclude '*/env'               \
    --exclude '*/tmp/*'             \
    --exclude '*/log'               \
    --exclude '*/snapshot.zip'      \
    --exclude '*/repositories/*'    \
    --exclude '*/cache/*'           \
    --exclude '*/__pycache__/*'     \
    --exclude '*/t/*'               \
    ::'{hostname}-{now}'            \
    /etc                            \
    /home                           \
    /root                           \
    /usr/local                      \
    /var                            \

echo "Restart mysqld"
service mysqld start

echo "Remove old backups"
borg prune                          \
    --prefix '{hostname}-'          \
    --keep-daily    6               \
    --keep-weekly   3               \
    --keep-monthly  5               \
