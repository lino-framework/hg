.. _getlino.install.prod:
.. _getlino.install.admin:
.. _lino.admin.install:

===================================
Set up a Lino production server
===================================

.. highlight:: console


.. _pip: http://www.pip-installer.org/en/latest/
.. _virtualenv: https://pypi.python.org/pypi/virtualenv

Here is a set of conventions we suggest to use as a :term:`server administrator` when
setting up a Lino :term:`production server`.

.. contents::
   :depth: 1
   :local:


General configuration
=====================

If your customers want to access their Lino from outside of their intranet, then
you need to setup a **domain name**.  See :doc:`/domain`. In this case you need
to have DNS set up and working because installing a Lino site will use certbot
to request a certificate.

Having a domain name is not necessary if you remain in your LAN, if you access
your Lino site via its IP adress, ...


Configure the default umask
===========================

All maintainers must have a umask `002` or `007` (not `022` or `077` as is the
default value).

Edit the file :file:`/etc/bash.bashrc` (site-wide for all users)::

    # nano /etc/bash.bashrc

And add the following line at the end::

    umask 002

The :cmd:`umask` command is used to mask (disable) certain file permissions from
any new file created by a given user. See :doc:`umask` for more detailed
information.

To activate the new :cmd:`umask` or for yourself, hit :kbd:`Ctrl+D` to end this session
and start a new session



.. _getlino.install.master:

Set up a master environment
===========================

If you are the first :term:`server administrator` on this :term:`server <production
server>`, you must set up the :term:`master environment`::

    $ sudo su
    # apt-get install pip virtualenv git
    # mkdir -p /usr/local/lino/shared/env
    # cd /usr/local/lino/shared/env
    # chown root:www-data .
    # chmod g+ws .
    # virtualenv -p python3 master
    # . /usr/local/lino/shared/env/master/bin/activate

Edit your :file:`/root/.bashrc` file and add the following line at the end so
that the master environment is activated also in future root sessions on this
server::

  # . /usr/local/lino/shared/env/master/bin/activate

Install the `wheel` python package, `wheel` is an installation tool for the pip
packages, there are many other installation tools, but most pip packages use
`wheel` to install the package into your python environment, some pip packages
might fail to install if `wheel` is NOT already installed::

  # pip install -U pip wheel

Install :mod:`getlino` into the :term:`master environment`::

  # pip install getlino


Run ``getlino configure``
=========================

Sign out and again in as root, and verify that your prompt shows that the
:term:`master environment` is activated::

  (master) root@myserver:~#

Run :cmd:`getlino configure` as root::

  # getlino configure --no-clone --appy --web-server nginx --https

The ``--web-server`` option can be either ``nginx`` or ``apache``. Your choice.
You might want to provide some extra arguments, for example, some database
related arguments are `--db-engine`, `--db-host`, `--db-port`, `--db-user`,
`--db-password`, to see all the available options see: :cmd:`getlino configure`.

The ``--https`` option causes :cmd:`getlino configure` to (1) install certbot
and (2) have it request a new certificate for every :cmd:`getlino startsite`.

When at least one :term:`Lino site` of a server uses :mod:`lino_xl.lib.appypod`,
then the server must have a LibreOffice service running so that the users of
your site can print documents using the appypdf, appydoc or appyrtf methods (the
appyodt method does not require a LO service). You say this using the
:cmd:`getlino configure --appy` option. For background information see
:doc:`oood`.

You may specify your answer to all those questions into the command line. For
example here is a variant of how to specify :setting:`admin_email` and
:setting:`languages`::

  # getlino configure --no-clone --appy --web-server nginx --https --admin-email root --languages "en de fr"

When you know that all the sites on your server will use the same environment,
you may tell getlino so by specifying the name of your :setting:`shared_env`::

  # getlino configure ... --shared-env /usr/local/lino/shared/env/miki


..
  $ sudo env PATH=$PATH getlino configure --no-clone --appy --web-server nginx --https

  Note: The ``env PATH=$PATH`` is needed to work around the controversial Debian
  feature of overriding the :envvar:`PATH` for security reasons (`source
  <https://stackoverflow.com/questions/257616/why-does-sudo-change-the-path>`__).

For details see the documentation about :ref:`getlino`.


Activate the master environment
===============================

For every new maintainer of a production site, add the following line to your
:xfile:`.bashrc` in order to have the :term:`master environment` activated each
time you connect to the server::

    . /usr/local/lino/shared/env/master/bin/activate

.. glossary::

  master environment

    A virtualenv to be used as default virtualenv for all :term:`site
    maintainers <server administrator>` on this :term:`production server`.  It mainly
    contains :ref:`getlino`.
    It is usually located in :file:`/usr/local/lino/shared/env`.


Check :xfile:`/etc/aliases`
===========================

Every production server should be able to send emails to its maintainers, e.g.
to notify them when a cron job fails.

::

  # apt install sendmail
  # nano /etc/aliases   # add your email address
  # newaliases


Install your first site
=======================

You will run :cmd:`getlino startsite` for every new site on your server::

    $ sudo su
    # getlino startsite APPNAME PRJNAME

Where:

- ``APPNAME`` is one of the Lino applications known by getlino (noi,
  cosi, avanti, voga...)

- ``PRJNAME`` is a unique internal name of your site on this server.

For example::

   # getlino startsite cosi first

And then point your browser to http://first.localhost

If something goes wrong, consult the :cmd:`getlino startsite` documentation.

Some useful additions to your shell
===================================

We suggest that you add the following to your system-wide
:file:`/etc/bash.bashrc`:

.. literalinclude:: bash_aliases

Test whether it works
=====================

Close and reopen your terminal to activate them.

Quickly go to a project directory and activate its Python environment::

  $ go prj1
  $ a

You can always try the following admin commands (they don't modify the database,
so they won't break anything)::

  $ pm status
  $ pm show users.AllUsers



Configure your mail system
==========================

A production site should be able to send emails at least to the :term:`server
administrator`. The default Lino configuration sends all emails to `localhost`.

If you know an SMTP server, then run :cmd:`sudo apt install postfix`, select
"Satellite system" and give the name of that SMTP server.

If you use :cmd:`monit`, then edit :file:`/etc/monit/monitrc` and add the
following line::

  set mailserver localhost


More options
============

You may want to set your server's timezone::

  $ sudo timedatectl set-timezone Europe/Tallinn

If you want to optimize the cron jobs created by getlino, see :doc:`/cron`.

If you want to :ref:`log every bash command <log2syslog>`, then add the
following to your system-wide :file:`/etc/bash.bashrc`

.. literalinclude:: log2syslog
