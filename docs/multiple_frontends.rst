.. _hosting.multiple_frontends:

===================================
Multiple front ends for a same site
===================================

You can configure two different Django projects (web sites) to serve a same
:term:`Lino site` using different :term:`front ends <front end>`. They will have
different domain names and nginx configurations, but share the same application
code and database.

The :xfile:`settings.py` file of one of them will import the settings of the
other one, and will basically just change the :attr:`default_ui
<lino.core.site.Site.default_ui>` setting.


- Use :cmd:`getlino startsite` to create the first Lino site::

    # getlino startsite noi mysite

- Create another directory called :xfile:`m`, next to the project directory::

    # go mysite
    # mkdir ../m
    # cp -a manage.py nginx ../m
    # ln -s env ../m
    # go m

In the project directory of the site, create a file named
:file:`settings.py` with this content::

  from lino_local.mysite.settings import *

  class Site(Site):
      master_site = SITE
      default_ui = "lino_react.react"

  SITE = Site(globals())

  ALLOWED_HOSTS = ...

Adapt the :xfile:`manage.py` and the :file:`nginx/uwsgi.ini` files (replace
"mysite" by "m").

Manually copy the config files for supervisor and nginx or apache.

Install the alternative front end into the virtualenv::

  $ . env/bin/activate
  $ pip install lino_react
