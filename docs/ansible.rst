========================
Our Ansible cheat sheet
========================

Note: we don't yet use Ansible efficiently, we are on our way to discover it and
we are not in a hurry.

Strange::

  [WARNING]: Platform linux on host xyz.mylino.net is using the discovered Python interpreter at /usr/bin/python, but future
  installation of another Python interpreter could change this. See
  https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.

Create a file :xfile:`~/.ansible.cfg`::

  [default]
  interpreter_python=auto_legacy_silent

But the warning doesn't go away...

How to build a Docker image that allows you to ssh into it::

  $ go hg
  $ cd projects/ansible
  $ docker build -t newserver -f Newserver ~/.ssh
  $ docker run -it newserver /bin/bash

External resources:

- `Ansible Tutorial for Beginners: Playbook & Examples <https://spacelift.io/blog/ansible-tutorial>`__
- `Terraform vs. Ansible : Key Differences and Comparison of Tools <https://spacelift.io/blog/ansible-vs-terraform>`__
