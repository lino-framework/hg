.. _hg.supervisor:

==========================
Our supervisor cheat sheet
==========================

This page collects know-how about using :term:`supervisor` on a :term:`Lino
server`.

.. glossary::

  supervisor

    A daemon used to monitor and control a number of processes at operating
    system level. Main documentation: http://supervisord.org

.. contents::
   :depth: 1
   :local:

Overview
========

On a :term:`production server`, the equivalent of :cmd:`pm runserver` is an asgi
process running under supervisor. For each :term:`Lino site` there is a separate
supervisor config file. The web server (:cmd:`nginx`) communicates with this
process via a Linux socket.

Configuration files created by getlino
======================================

.. currentmodule:: getlino

When either :data:`web_server` or :data:`appy` (or both) is defined in the
:ref:`configuration files <getlino.config>`, :cmd:`getlino configure` will

- install the ``supervisor`` system package,
- create a logrotate configuration file (:file:`/etc/logrotate.d/supervisor.conf`) for :file:`/var/log/supervisor/supervisord.log`,
- create a :xfile:`libreoffice.conf` file in the :data:`supervisor_dir`,

and :cmd:`getlino startsite` will install a supervisor config file for the site.


Diagnose
========

See the status of supervisor::

  $ sudo service supervisor status

Example output::
  
  ● supervisor.service - Supervisor process control system for UNIX
       Loaded: loaded (/lib/systemd/system/supervisor.service; enabled; vendor preset: enabled)
       Active: active (running) since Mon 2024-11-25 10:22:11 UTC; 22s ago
         Docs: http://supervisord.org
     Main PID: 185508 (supervisord)
        Tasks: 12 (limit: 2300)
       Memory: 848.9M
          CPU: 20.056s
       CGroup: /system.slice/supervisor.service
               ├─185508 /usr/bin/python3 /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf
               ├─185509 /usr/local/lino/shared/env/pilv/bin/python /usr/local/lino/shared/env/pilv/bin/daphne -u /run/daphne/alpha_daphne0.sock --fd 0 --access-log - --proxy-headers asgi:app>
               ├─185510 /usr/local/lino/shared/env/pilv/bin/python /usr/local/lino/shared/env/pilv/bin/daphne -u /run/daphne/beta_daphne0.sock --fd 0 --access-log - --proxy-headers asgi:appl>
               ├─185511 python manage.py linod --force
               ├─185512 python manage.py linod --force
               ├─185513 /usr/lib/libreoffice/program/oosplash --accept=socket,host=127.0.0.1,port=8100;urp; --nologo --headless --nofirststartwizard
               └─185542 /usr/lib/libreoffice/program/soffice.bin --accept=socket,host=127.0.0.1,port=8100;urp; --nologo --headless --nofirststartwizard


Possible issues
===============

Main process exited
-------------------

When the status says

- Process: 184306 ExecStart=/usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf (code=exited, status=1/FAILURE)

Followed later by

- Main process exited, code=exited, status=1/FAILURE".

You can manually invoke the specified command as root in order to see what
happens::

  $ sudo /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf

Could not create FastCGI socket
-------------------------------

Possible error message during :cmd:`supervisord` startup:

- Could not create FastCGI socket tcp://127.0.0.1:8003: [Errno 98] Address
  already in use

This means that the process who owns this socket hasn't been stopped for some
reason. You can show a list of candidates::

  $ ps aux | grep python

Or you can use :cmd:`netstat` to get more information about which process owns a
given socket::

  $ sudo netstat -tulpen

Example output on a running Lino site::

  Active Internet connections (only servers)
  Proto  Local Address   Foreign Address State   User  Inode    PID/Program name
  ...
  tcp    127.0.0.1:8001  0.0.0.0:*       LISTEN  0     3660773  184777/python3
  tcp    127.0.0.1:8003  0.0.0.0:*       LISTEN  0     3660776  184777/python3
  tcp    127.0.0.1:8100  0.0.0.0:*       LISTEN  0     1217773  58122/soffice.bin
  tcp    127.0.0.1:3306  0.0.0.0:*       LISTEN  108   3534434  177680/mariadbd
  tcp    127.0.0.1:6379  0.0.0.0:*       LISTEN  109   11902    560/redis-server 12
  ...
