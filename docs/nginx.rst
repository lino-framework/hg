.. _hosting.nginx:
.. highlight:: console

=====================
Our nginx cheat sheet
=====================

.. xfile:: .conf
.. xfile:: nginx.conf

An nginx config file. All config files are in :file:`/etc/nginx/sites-available`
and must be named with the suffix :xfile:`.conf`.

To actually activate them, you must also create a symbolic link to each of them
in :file:`/etc/nginx/sites-enabled`. This Debian-specific convention is okay for
us. It has the advantage that we can easily disable individual sites or do
temporary magics.

Our additional convention is to name them `xxx.conf`, where `xxx` matches the
subdomain they serve.

Static websites
===============

The main website serves at two subdomains `www.example.com` and `example.com`.
When somebody types just `example.com` (without `www`) in their browser, we
permanently redirect them to the canonical `www.example.com`. Here is a template
for a typical :file:`www.conf` file::


  server {
    server_name www.example.com example.com;
    if ($host = example.com) { return 301 https://www.$host$request_uri; }
    root /path/to/www;
    index index.html;
    listen 80;
  }

For additional subdomains the :xfile:`.conf` file is even shorter::

  server {
    server_name foo.example.com;
    root /path/to/foo;
    index index.html;
    listen 80;
  }

How to add a new subdomain
==========================

You have already a website `foo.example.com` and now want to have another
website `bar.example.com`::

  $ sudo nano /etc/nginx/sites-available/bar.conf

Paste content from above templates and edit as needed::

  $ sudo su
  # cd /etc/nginx/sites-enabled
  # ln -s /etc/nginx/sites-available/bar.conf .
  # service nginx restart
  # certbot --nginx -d bar.example.com

Note that the first filename given to :cmd:`ln -s` should be the full path.

.. _hosting.invalid_http_host_header:

Invalid HTTP_HOST header
========================

Problem: A new production server is sending lots of emails to :setting:`ADMINS`
saying [mysite] ERROR (EXTERNAL IP): Invalid HTTP_HOST header: '12.123.234.56'.
You may need to add '12.123.234.56' to ALLOWED_HOSTS.

Explanation: there are robots called IP port scanners, both legitimate and
suspicious, that try to get an answer from every IP address. The problem comes
when nginx lets them pass though until Lino. Lino should not get bothered with
such requests. But Lino *is* getting bothered because we haven't defined any
default server. These requests have no Host header.

The solution (found `here
<https://nginx.org/en/docs/http/request_processing.html>`__) is to define a
default server in your :xfile:`prod.conf` file. Something like this::

  server {
     listen      80 default_server;
     listen 443  default_server ssl;
     server_name "12.123.234.56";
     ssl_certificate /etc/letsencrypt/live/bar.example.com/fullchain.pem; # managed by Certbot
     ssl_certificate_key /etc/letsencrypt/live/bar.example.com/privkey.pem; # managed by Certbot
     return      444;
  }
