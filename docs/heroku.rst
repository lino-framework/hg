.. hg.heroku

=============================
Running a Lino site on Heroku
=============================

This tutorial is Ubuntu 16+ specific. If you are using other platforms most of
the instructions will remain the same and some other instructions may vary to
some degree. To understand more about heroku, visit `heroku devcenter
<https://devcenter.heroku.com/articles/getting-started-with-python>`_. If you
are having trouble following this tutorial, contact us for help.

- Create an account at Heroku:

    If you  do not have an account on `heroku <https://heroku.com>`_, click on
    this `link <https://signup.heroku.com>`_ to fill up the form and follow the
    flow to create an account of heroku.

- Install heroku on your local machine:

    If you are using Ubuntu 16+ then use the following command to install
    heroku::

        $ sudo snap install heroku --classic

    Otherwise follow the instruction on heroku `devcenter
    <https://devcenter.heroku.com/articles/getting-started-with-python#set-up>`_.

- Heroku CLI login:

    Use the following command::

        $ heroku login

    This will open a page in your browser. Fill up the login information and
    `Log In` to heroku. This will log you on in heroku CLI.

- Get a clone of the `heroku-template <https://gitlab.com/lino-framework/heroku-template>`_ repository from GitLab::

    $ git clone https://gitlab.com/lino-framework/heroku-template.git
    $ cd heroku-template

- Create a heroku app::

    $ heroku create my-lino-app

- Modify settings.py and heroku config:

    Set the **ALLOWED_HOSTS** variable in amici1/settings.py file to include our
    app hostname (is our case this would be) `my-lino-app.herokuapp.com`. The
    value for the ALLOWED_HOSTS variable should be::

        ALLOWED_HOSTS = ['my-lino-app.herokuapp.com']

    Set the :attr:`Site.title` attribute to "my-lino-app".

- Deploy on heroku::

    $ git push heroku master

After when the final command is done, you can access your app visiting the url
https://my-lino-app.herokuapp.com/
