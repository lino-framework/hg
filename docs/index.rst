Welcome to the **Lino Hosting Guide**, the website for
for :term:`server administrators <server administrator>` who set
up or maintain a :term:`Lino server`.


.. _hg:
.. _lino.hosting:
.. _lino.hosters:
.. _lino.admin:

==================
Lino Hosting Guide
==================

Deploying a Lino site is the same as `Deploying a Django project
<https://docs.djangoproject.com/en/5.0/howto/deployment/>`__. If you are already
hosting Django projects, then you maybe don't need this guide because you have
your established approaches. But even then you might find inspiration for your
system by reading how we do it.

.. toctree::
    :maxdepth: 1

    start
    /topics/index
    beyond
    copyright

.. toctree::
  :hidden:

  links
